﻿using System;
using AttendancePOC.Constants;
using AttendancePOC.Gateways.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AttendancePOC.Services;
using AttendancePOC.Services.Impl;

namespace AttendanceTrayAppTests
{
    [TestClass]
    public class AttendanceTrayAppTest
    {
        [TestMethod]
        public void GetLastLoginDateServiceTest()
        {
            Mock<IInitiateTimersService> mockTimerss=new Mock<IInitiateTimersService>();
            Mock<ILastLoginDetailService> mockLoginDetailService = new Mock<ILastLoginDetailService>();
            mockLoginDetailService.Setup(x => x.GetLastLoginDate()).Returns("2/21/2017");
            var date = DateTime.Now.Date;
            var lastLoginDate=new LastLoginDetailService(mockTimerss.Object);
            lastLoginDate.SetLastLoginDate(date);
            var loginDate = lastLoginDate.GetLastLoginDate();
            lastLoginDate.CheckUserLoggedInDetails(DateTime.Parse(loginDate));
            Assert.IsTrue(loginDate.Contains("2/22/2017"));

        }

        [TestMethod]
        public void InitiateTimerserviceTest()
        {
            Mock<IInitiateTimersService> mockTimerss = new Mock<IInitiateTimersService>();
            mockTimerss.Setup(x => x.LoginPromptTimer());
            mockTimerss.Object.LoginPromptTimer();

        }
        [TestMethod]
        public void Test()
        {
            var gateway=new AttendanceGateWay(new ApplicationSettings(),new Constants());
            var response = gateway.GetLatestAttendanceForUser("sramanathan");
            Assert.IsNotNull(response);
        }
    }
}
