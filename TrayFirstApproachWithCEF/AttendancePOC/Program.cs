﻿using System;
using System.Reflection;
using System.Windows.Forms;
using AttendancePOC.Services.Impl;
using log4net;
using Microsoft.Win32;
using Ninject;
using Yort.Ntp;

namespace AttendancePOC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
         

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;
            Application.Run(new AttendanceTrayApp(new ApplicationSettings(), new Constants.Constants()));
        }

        static void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            var kernel=new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            var initiateTimer = kernel.Get<InitiateTimersService>();

            if (e.Reason != SessionSwitchReason.SessionUnlock) return;
            var currentTime = KnownNtpServers.Asia;
            var npNtpClient = new NtpClient(currentTime);
            var currentDate = npNtpClient.RequestTimeAsync().Result.Date;

            var lastLoginDetailService = new LastLoginDetailService(initiateTimer);
            lastLoginDetailService.CheckUserLoggedInDetails(currentDate);
            Log.Debug("System Unlocked");
        }
    }
}
