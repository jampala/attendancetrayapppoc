﻿using CefSharp.WinForms;
using System;
using System.Drawing;
using System.Windows.Forms;
using AttendancePOC.Services.Impl;

namespace AttendancePOC
{
    public partial class AttendanceTrayApp : Form
    {
        private ChromiumWebBrowser _browser;
        private readonly ApplicationSettings _applicationSettings;
        private readonly Constants.Constants _constants;

        public AttendanceTrayApp(ApplicationSettings applicationSettings,Constants.Constants constants)
        {
            _applicationSettings = applicationSettings;
            _constants=constants;
            InitializeComponent();
            InItBrowser();
            WindowState=FormWindowState.Minimized;
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - 510,
                                      workingArea.Bottom - 530);
            ShowInTaskbar = false;
            notifyIcon1.Visible = true;
        }

        public void InItBrowser()
        {
            var url= $"{_applicationSettings.RemoteUrl}{_constants.PageRouteAttendance}";
            _browser = new ChromiumWebBrowser(url)
            {
                Dock = DockStyle.Fill,
            };
              Controls.Add(_browser);
        }

        private void AttendanceTrayApp_Closing(object sender, FormClosingEventArgs e)
        {
            
            
            if (WindowState != FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
                WindowState =FormWindowState.Minimized;
                
                e.Cancel = true;
            }
            //browser.
            //Cef.Shutdown();
        }

        private void notifyIcon1_MouseDoubleClick_1(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = true;
        }

        private void notifyIcon1_MouseSingleClick_1(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = true;
        }

        public void PopupTheWebBrowser()
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = true;
        }
    }
}
