﻿namespace AttendancePOC.Models
{
    public class ApiResponse
    {
        public LatestAttendance Data { get; set; }
        public string IsError { get; set; }
    }
}
