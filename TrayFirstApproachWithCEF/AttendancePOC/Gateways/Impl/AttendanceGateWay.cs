﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AttendancePOC.Models;
using AttendancePOC.Services.Impl;
using Newtonsoft.Json;

namespace AttendancePOC.Gateways.Impl
{
    public class AttendanceGateWay:IAttendanceGateWay
    {
        private readonly ApplicationSettings _applicationSettings;
        private readonly Constants.Constants _constants;
        public AttendanceGateWay(ApplicationSettings applicationSettings,Constants.Constants constants)
        {
            _applicationSettings = applicationSettings;
            _constants = constants;
        }
        private static readonly HttpClient Client = new HttpClient();
        public ApiResponse GetLatestAttendanceForUser(string ntLogin)
        {
            using (HttpClient client = new HttpClient())

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var url =
              new StringBuilder(_applicationSettings.RemoteUrl).AppendFormat("{0}{1}{2}",_constants.ApiHasUserLoggedInToday, "?NTLogin=", ntLogin)
                  .ToString();
            var response= Client.GetAsync(url).Result;
            var attendanceResponse = JsonConvert.DeserializeObject<ApiResponse>(response.Content.ReadAsStringAsync().Result);
            return attendanceResponse;
        }
    }
}
