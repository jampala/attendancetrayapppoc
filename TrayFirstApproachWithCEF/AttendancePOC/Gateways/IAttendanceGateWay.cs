﻿using AttendancePOC.Models;

namespace AttendancePOC.Gateways
{
    interface IAttendanceGateWay
    {
        ApiResponse GetLatestAttendanceForUser(string ntLogin);
    }
}
