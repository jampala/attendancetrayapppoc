﻿using System;

namespace AttendancePOC.Services
{
    public interface ILastLoginDetailService
    {
        string GetLastLoginDate();
        void SetLastLoginDate(DateTime currentDate);
    }
}
