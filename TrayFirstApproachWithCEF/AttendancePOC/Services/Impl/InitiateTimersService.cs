﻿using System;
using AttendancePOC.Gateways.Impl;
using log4net;
using static System.Int32;

namespace AttendancePOC.Services.Impl
{
    public class InitiateTimersService:IInitiateTimersService
    {
        private System.Windows.Forms.Timer _timer;
        private readonly ApplicationSettings _applicationSettings;
        private readonly ILog _log = LogManager.GetLogger(typeof(Program));
        private readonly AttendanceTrayApp _attendanceTrayApp;
        private readonly AttendanceGateWay _attendanceGateWay;

        public InitiateTimersService(ApplicationSettings applicationSettings,System.Windows.Forms.Timer timer, AttendanceTrayApp attendanceTrayApp,AttendanceGateWay attendanceGateWay)
        {
            _applicationSettings = applicationSettings;
            _timer = timer;
            _attendanceTrayApp = attendanceTrayApp;
            _attendanceGateWay = attendanceGateWay;
        }
        public void LoginPromptTimer()
        {
            ValidateAttendanceOfLogin();
            if (!_timer.Enabled)
            {
                _timer = new System.Windows.Forms.Timer
                {
                    Enabled = true,
                    Interval = Parse(_applicationSettings.LoginIntervalTime)
                };
                _timer.Tick += LoginPromptTimerEvent;
                _log.Debug("Timer has been setted for 2 min");
            }
           

        }

        public void IntervalPromptTimer()
        {
            if (!_timer.Enabled)
            {
                _timer = new System.Windows.Forms.Timer
                {
                    Enabled = true,
                    Interval = Parse(_applicationSettings.PopupIntervalTime)
                };
                _timer.Tick += LoginPromptTimerEvent;
                _log.Debug("Timer has been setted for 2 hr");
            }
            
        }

        private void ValidateAttendanceOfLogin()
        {
            string ntLogin = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            var response=_attendanceGateWay.GetLatestAttendanceForUser(ntLogin);
            if (response.Data.HasUserLoggedIn == "True")
            {
                IntervalPromptTimer();
            }
        }

        private void LoginPromptTimerEvent(object sender, EventArgs e)
        {
            _attendanceTrayApp.PopupTheWebBrowser();
            _log.Debug("CEF Browser Prompted");
            _timer.Enabled = false;
        }
    }
}
