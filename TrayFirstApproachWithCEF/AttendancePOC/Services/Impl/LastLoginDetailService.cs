﻿using System;
using System.Globalization;
using AttendancePOC.Properties;
using log4net;

namespace AttendancePOC.Services.Impl
{
    public class LastLoginDetailService : ILastLoginDetailService
    {
        private readonly IInitiateTimersService _initiateTimersService;
        private readonly ILog _log = LogManager.GetLogger(typeof(Program));

        public LastLoginDetailService(IInitiateTimersService initiateTimersService)
        {
            _initiateTimersService = initiateTimersService;
        }

        public string GetLastLoginDate()
        {
            Settings.Default.LastLoginDate = "";
            Settings.Default.Save();
            return Settings.Default.LastLoginDate;
        }

        public void SetLastLoginDate(DateTime currenDateTime)
        {
            Settings.Default.LastLoginDate = currenDateTime.ToString(CultureInfo.InvariantCulture);
            Settings.Default.Save();
        }

        public void CheckUserLoggedInDetails(DateTime currentDate)
        {
            var lastLoginDate = GetLastLoginDate();
            if (!(currentDate.Date.Day == DateTime.Now.Day && lastLoginDate.Contains(currentDate.ToString("d",CultureInfo.InvariantCulture))))
            {
                SetLastLoginDate(currentDate);
                //Start Timer for 2 min
                _initiateTimersService.LoginPromptTimer();
            }
            _log.Debug("Validated Login Date for "+currentDate);
        }
    }
}
