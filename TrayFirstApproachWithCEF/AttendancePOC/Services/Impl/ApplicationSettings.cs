﻿using System.Configuration;

namespace AttendancePOC.Services.Impl
{
    public class ApplicationSettings:IApplicationSettings
    {
        public string LoginIntervalTime => ConfigurationManager.AppSettings["LoginIntervalTime"];
        public string PopupIntervalTime => ConfigurationManager.AppSettings["popupIntervalTime"];
        public string DateFilePath => ConfigurationManager.AppSettings["DateFilePath"];
        public string RemoteUrl=>ConfigurationManager.AppSettings["RemoteUrl"];
    }
}
