﻿using System;

namespace AttendancePOC.Services
{
    public interface IInitiateTimersService
    {
        void LoginPromptTimer();
        void IntervalPromptTimer();
    }
}
