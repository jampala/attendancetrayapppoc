﻿namespace AttendancePOC.Services
{
    public interface IApplicationSettings
    {
        string LoginIntervalTime { get; }
        string DateFilePath { get; }
    }
}
