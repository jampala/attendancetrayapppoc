﻿using System.Windows.Forms;
using CefSharp.WinForms;

namespace AttendancePOC
{
    partial class AttendanceTrayApp
    {
        private static readonly string IconFileName = "../../../Resource/SpiderLogo.ico";
        private static readonly string DefaultTooltip = "Spider CustomApplicationContext App";
        private NotifyIcon notifyIcon1;
        //private ChromiumWebBrowser browser;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1 = new NotifyIcon(this.components)
            {
                ContextMenuStrip = new ContextMenuStrip(),
                Icon= new System.Drawing.Icon(IconFileName),
                Text=DefaultTooltip,
                Visible=true
                
            };
            notifyIcon1.DoubleClick += notifyIcon1_MouseDoubleClick_1;
            notifyIcon1.Click += notifyIcon1_MouseSingleClick_1;
            this.SuspendLayout();
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.Name = "AttendanceTrayApp";
            this.Text = "AttendanceTrayApp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AttendanceTrayApp_Closing);
            this.ResumeLayout(false);
        }
        #endregion
    }
}

