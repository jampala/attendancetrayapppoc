﻿using AttendancePOC.Services;
using AttendancePOC.Services.Impl;
using Ninject.Modules;

namespace AttendancePOC.Bindings
{
    public class InitiateTimersBinding:NinjectModule
    {
        public override void Load()
        {
            Bind<IInitiateTimersService>().To<InitiateTimersService>();
        }
    }
}
