﻿namespace AttendancePOC.Constants
{
    public class Constants
    {
        public string PageRouteAttendance = "/#/attendance";
        public string ApiHasUserLoggedInToday = "/api/hasUserLoggedInToday/";
    }
}
