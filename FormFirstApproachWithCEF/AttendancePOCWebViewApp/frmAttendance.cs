﻿using System;
using System.Threading;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using Timer = System.Windows.Forms.Timer;

namespace AttendancePOCWebViewApp
{
    public partial class frmAttendance : Form
    {
        private Timer timer;
        private int startPosX;
        private int startPosY;
        public ChromiumWebBrowser browser;
        public frmAttendance()
        {
            InitializeComponent();
           

            // Simply pass the URL you wish to navigate to, to the 'Load' method
            
            //SetUpTimer(TimeSpan.FromHours(16)+TimeSpan.FromMinutes(34));
            //timer = new Timer();
            //timer.Interval = 10;
            //timer.Tick += timer_Tick;
            //timer.Enabled = true;
            //InitializeChromium();
            //this.Load += new EventHandler(frmAttendance_Load);
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            //Lift window by 5 pixels
            startPosY -= 20;
            //If window is fully visible stop the timer
            if (startPosY < Screen.PrimaryScreen.WorkingArea.Height - Height)
                timer.Stop();
            else
                SetDesktopLocation(startPosX, startPosY);
        }
        private System.Threading.Timer timer1;
        private void SetUpTimer(TimeSpan alertTime)
        {
            DateTime current = DateTime.Now;
            TimeSpan timeToGo = alertTime - current.TimeOfDay;
            if (timeToGo < TimeSpan.Zero)
            {
                return;//time already passed
            }
            this.timer1 = new System.Threading.Timer(x =>
            {
                this.frmAttendance_Load(timer,EventArgs.Empty);
            }, null, timeToGo, Timeout.InfiniteTimeSpan);
        }

        //public ChromiumWebBrowser chromeBrowser;

        //    public void InitializeChromium()
        //{
        //    CefSettings settings = new CefSettings();
        //    // Initialize cef with the provided settings
        //    Cef.Initialize(settings);
        //    // Create a browser component
        //    chromeBrowser = new ChromiumWebBrowser("http://localhost:3000/#/hackin");
        //    // Add it to the form and fill it to the form window.
        //    this.Controls.Add(chromeBrowser);
        //    chromeBrowser.Dock = DockStyle.Fill;
        //}

        //protected override void OnLoad(EventArgs e)
        //{
        //    // Move window out of screen
        //    startPosX = Screen.PrimaryScreen.WorkingArea.Width - Width;
        //    startPosY = Screen.PrimaryScreen.WorkingArea.Height;
        //    SetDesktopLocation(startPosX, startPosY);
        //    base.OnLoad(e);
        //    // Begin animation
        //    timer.Start();
        //}
        private void frmAttendance_Load(object sender, System.EventArgs e)
        {
            //timer.Start();
            lblGreeting.Text = string.Format("HELLO, " + SystemInformation.ComputerName);
            browser = new ChromiumWebBrowser("http://spiderlms.azurewebsites.net/#/signin")
            {
                Dock = DockStyle.Fill,
            };
            Controls.Add(browser);
            /// it will block the right click on browser
            //browser.MenuHandler = new CustomMenuHandler();

            //browser.Load("http://spiderlms.azurewebsites.net/#/signin");
            //webBrowser1.Navigate("http://localhost:3000/#/hackin");
            //startPosX = Screen.PrimaryScreen.WorkingArea.Width - Width;
            //startPosY = Screen.PrimaryScreen.WorkingArea.Height;
            // SetDesktopLocation(startPosX, startPosY);
            //var thread = new Thread(new ThreadStart(timer.Start));


        }

        private void notifyIcon1_MouseDoubleClick_1(object sender, MouseEventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = true;
        }

        private void frmAttendance_Resize(object sender, System.EventArgs e)
        {
            //if the form is minimized  
            //hide it from the task bar  
            //and show the system tray icon (represented by the NotifyIcon control)  
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void frmAttendance_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            //browser.
            Cef.Shutdown();
        }
    }
}
